
var url = "http://api.nbp.pl/api/exchangerates/tables/A/?format=json";

var itemsPerPage = 5;

// global arrays keeping data original and sorted
var dataArrayGlobal = [];
var dataArrayGlobalFiltered = [];

// global data for pagination + initial page number
var page_number = 1;
var nrOfPages;




// getting data form Json, putting into array and append in view
function getData(){
    $.getJSON( url )

    .done (function(data){
        var quanity = data[0].rates.length;
        var dataArray = makeArray(data, quanity);
        dataArrayGlobal = dataArray;
        dataArrayGlobalFiltered = dataArray;
        
        printData(dataArray, itemsPerPage);
    })
    .fail (function(){
        $('.body').append('<div class="errorMessage">Błąd połączenia, nie można pobrać danych z systemu.</div>');
    });
}


// making array based on object
function makeArray (data, quanity) {
    var dataArray = [];
    var i = 0;
    while(i < quanity) {
        dataArray.push({code: data[0].rates[i].code, currency: data[0].rates[i].currency, rates: data[0].rates[i].mid});
        i++;
    } 
    return dataArray;
}


// sorting array
function sortArray(dataArray, by, way) {
    dataArray.sort(function(a, b){
        if (typeof a[by] === 'string') {
            if (a[by].toUpperCase() < b[by].toUpperCase()) return -1;
            if (b[by].toUpperCase() < a[by].toUpperCase()) return 1;
        } else {
            if (a[by] < b[by]) return -1;
            if (b[by] < a[by]) return 1;
        }
        return 0;
    });
    
    if (way === "DESC") {dataArray.reverse();};
    return dataArray;
}


// sorting data
function sortData(by, way){
    $('.body').empty();
    sortArray(dataArrayGlobalFiltered, by, way);
    sortArray(dataArrayGlobal, by, way);
    
    printData(dataArrayGlobalFiltered, itemsPerPage);
}


// appending data to view
function printData (dataArray, itemsPerPage) {
    
    if (dataArray.length >0) {

        var end = page_number * itemsPerPage;
        var start = end - itemsPerPage+1;
        var i = start;
        
        // checking if end variable is bigger the array length 
        if (end > dataArray.length) {end = dataArray.length;};
        
        while( end >= i ) {
            var val = dataArray[i-1];
            $('.body').append( 
                '<div class="item">'+
                    '<div class="img">'+
                        '<img src="img/flags/' + val.code.substring(0,2).toLowerCase() +'.png" alt="' + val.code +'" onError="this.src=\'img/flags/not_found.png\';"; />'+
                    '</div>'+
                    '<div class="code">' + val.code +'</div>'+
                    '<div class="ammount">'+ val.rates+'</div>'+
                    '<div class="name">' +  val.currency.toUpperCase() +'</div>'+
                '</div>'
            );
            i++;
        };
       
        showPagination();
        
    } else {
        $('.body').append( '<div class="errorMessage">Brak wyników</div>' );
    }
}


// search in array
function find(array, str, by){
    var tempArray = [];
    
    $.each(array, function(index, val) {
        if ( (val[by]+"").indexOf(str) > -1 ) {
            tempArray.push({code: val.code, currency: val.currency, rates: val.rates});
        } 
    });
    return tempArray;
}


// resetting search 
function resetSearch () {
    
    $('.search-container').slideUp('fast');
     
    $( '#inputCode' ).val('');
    $( '#inputName' ).val('');
    filter();
}


// applying filter
function filter () {
    
    $('.body').empty();
  
    $('#choiceCode span').html($( '#inputCode').val().toUpperCase());
    $('#choiceName span').html($( '#inputName').val().toLowerCase());
    
    if ($( '#inputCode' ).val() !== '' || $( '#inputName').val() !== '') {
        $('.filteredContainer').slideDown('fast');} else {$('.filteredContainer').slideUp('fast');
    };
    
    if ($( '#inputCode' ).val() !== '') {
        $( '#choiceCode' ).slideDown('fast');
    } else {$( '#choiceCode' ).slideUp(0);}

    if ($( '#inputName' ).val() !== '') {
        $( '#choiceName' ).slideDown('fast');
    } else {$( '#choiceName' ).slideUp(0);}
     
    // first find code in global array in order to clear last search
    dataArrayGlobalFiltered = find(dataArrayGlobal,$( '#inputCode').val().toUpperCase(), 'code');
   
    // then search currency in earlier filtered array 
    dataArrayGlobalFiltered = find(dataArrayGlobalFiltered, $('#inputName').val().toLowerCase(), 'currency');

    // print filtered array
    printData(dataArrayGlobalFiltered, itemsPerPage);
    
    showPagination();
    
    // showing number of results
    $('#searchQuanity').html( dataArrayGlobalFiltered.length );
}


// appending pagination 
function showPagination () {
    nrOfPages = Math.ceil(dataArrayGlobalFiltered.length / itemsPerPage);

    // if nr of pages is 0 then hide pagination container
    nrOfPages < 1 ? $('.pagination-container').hide(0) : $('.pagination-container').show(0);
    
    $('#paginItems').empty();

    // appending pagination buttons 
    for (var n=1; n <= nrOfPages; n++) {
        $('#paginItems').append( '<div class="pagin anim" data-page="'+ n +'">'+ n +'</div>' );
    }

    // making pagination button selected
    $('.pagin[data-page="'+page_number+'"]').addClass('active');


    // setting next/prev buttons active/disabled
    $('.paginExtreme').removeClass('notActive');
    if (page_number === 1) {$('.paginExtreme.first').addClass('notActive');};
    if (page_number == nrOfPages) {$('.paginExtreme.last').addClass('notActive');};
};


// changing active page
function pageChange () {

    $('.body').empty();
    $('.pagin').removeClass('active');
    $('.pagin[data-page="'+page_number+'"]').addClass('active');

    printData (dataArrayGlobalFiltered, itemsPerPage);
};







$( document ).ready(function() {

    // get and show data on load
    getData();

    // hiding filtered choice 
    $('#choiceCode').hide(0);
    $('#choiceName').hide(0);


    // sorting data 
    $('.dSort').click(function(event){
        event.preventDefault();

        // get the 'way' and 'by' from buttons and pass to sort function 
        var way = $(this).data('way');
        var by = $(this).data('by');
        sortData(by, way);

        $(this).data('way')  === 'ASC' ? $(this).data('way', 'DESC') : $(this).data('way', 'ASC');

        // adding class to show which sort is applied 
        $('.dSort').removeClass( "activeASC" );
        $('.dSort').removeClass( "activeDESC" );
        $(this).addClass( "active" + $(this).data('way') );
    });
    

    $('.search').click(function(){
        $('.search-container').slideToggle('fast');
    });
    

    // searching based on form values
    $('#searchForm').submit(function(event){
        $('.search-container').slideUp('fast');
        page_number = 1;

        filter ();
        event.preventDefault();
    });
    

    // resetting search
    $('.filterReset, #choiceReset').click(function(){
        resetSearch();
    });
    

    // removing filter by 'code'
    $('#choiceCode a').click(function(){

         // toogle visibility of search container
        $( '.search-container' ).slideUp('fast');
        $( '#choiceCode' ).fadeOut('fast');
        $( '#inputCode' ).val('');

        filter();
    });
    

    // removing filter by 'name'
    $('#choiceName a').click(function(){

         // toogle visibility of search container
        $('.search-container').slideUp('fast');
        $( '#choiceName' ).fadeOut('fast');
        $( '#inputName').val('');

        filter();
    });
    

    // pagination next page 
    $('.paginExtreme.last').click(function(event){
        event.preventDefault();
        if (page_number < nrOfPages) {page_number ++;}
        pageChange ();
    });
    

    // pagination previous page 
    $('.paginExtreme.first').click(function(event){
        event.preventDefault();
        if (page_number > 1) {page_number --;}
        pageChange ();
    });
    

    // add on click handler to page number buttons 
    $(document).on('click', '.pagin', function(event){ 
        event.preventDefault();
        page_number = $(this).data('page');
        pageChange ();
    });
    
});




